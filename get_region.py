import os
import openpyxl
from openpyxl import Workbook
from openpyxl import load_workbook
from openpyxl import workbook
import logging

# env workdir
data_folder = os.environ.get('excel_data_path', './data/')

logging.basicConfig(filename="/tmp/rw_excel.log", level=logging.INFO)

def return_ws(file_name, sheet_name):

    print("Открыть книгу: ", file_name)

    logging.info("Открыть книгу: " + data_folder + file_name)

    try:
        wb = load_workbook(data_folder + file_name,
                           read_only=False, data_only=True)
    except Exception:
        print("Файл: " + data_folder + file_name + " не найден!")
        logging.info("Файл: " + data_folder + file_name + " не найден!")
        return {}, {'code': 1, 'msg': "Файл: " + data_folder + file_name + " не найден!"}

    print("Выбрать лист: ", sheet_name)

    if sheet_name in wb.sheetnames:
        ws = wb[sheet_name]
    else:
        sheetindexs = {wb.index(sheet): sheet for sheet in wb.worksheets}
        ws = sheetindexs.get(0, None)
    
    return {'wb': wb, 'ws': ws}, {'code': 0}

# Export func
def get_region_data(file_name, sheet_name, up, left, bottom, right):
    
    err = {'code': 0}

    file, err = return_ws(file_name, sheet_name)
    
    if err['code'] == 1:
        return [], err['msg']
    
    if err['code'] == 2:
        file['wb'].close()
        return [], err['msg']

    ws = file['ws']
    
    print("Лист открыт")

    up      = int(up)
    left    = int(left)
    bottom  = int(bottom)
    right   = int(right)

    if bottom == 0:
        bottom = ws.max_row
    
    if right == 0:
        right = ws.max_column
    
    if bottom > ws.max_row:
        bottom = ws.max_row
    
    if right > ws.max_column:
        right = ws.max_column

    up      = max(up, 0)
    left    = max(left, 0)
    bottom  = max(bottom, 0)
    right   = max(right, 0)

    #rows_iter = ws.iter_rows(min_col=left, min_row=up, max_col=right, max_row=bottom)
    #data = [[cell.value for cell in row] for row in rows_iter]

    data = [['' if i.value is None else str(i.value) for i in j] for j in ws.get_squared_range(left, up, right, bottom)]

    file['wb'].close()   

    return data, err

def get_names_data(file_name, sheet_name):

    logging.info("get_names_data: " + sheet_name)

    err = {'code': 0}

    file, err = return_ws(file_name, sheet_name)

    if err['code'] == 1:
        return [], err['msg']

    if err['code'] == 2:
        file['wb'].close()
        return [], err['msg']

    ws = file['ws']
    wb = file['wb']

    wb.active

    print("Лист открыт")
    logging.info("Лист открыт")

    data = []

    for definedName in wb.defined_names.definedName:

        range = list(definedName.destinations)[0]
        address = range[1].replace('$', '').split(':')

        if sheet_name != range[0]:
            continue

        if len(address) == 1:
            cell = ws[address[0]]
            data.append({'name': definedName.name, 'value': cell.value, 'adress': address[0]})
        elif 'Область_Таблица_' in definedName.name:

            cells_range = list(definedName.destinations)[0][1]
            cells = ws[cells_range][0]
            name_table_range = [cell.value for cell in cells]

            data.append({'name': definedName.name+'_Заголовок', 'name_table_range': name_table_range, 'adress': cells_range})

            left = ws[address[0]].col_idx
            up = ws[address[0]].row + 1
            right = ws[address[1]].col_idx
            bottom = ws.max_row
            #data_rang = [[i.value for i in j] for j in ws.get_squared_range(left, up, right, bottom)]
            #squared_range = [[i for i in j] for j in ws.iter_cols(min_col=left, max_col=right, min_row=up, max_row=bottom)]
            squared_range = [[i for i in j] for j in ws.get_squared_range(left, up, right, bottom)]
            data_rang = list()

            column_type = {}

            b = 0
            for j in squared_range:
                row_rang = list()
                for i in j:

                    if b == 0:

                        find_type = False
                        for dV in ws.data_validations.dataValidation:
                            if find_type == True:
                                break
                            if dV.cells.ranges[0].max_col == i.col_idx and dV.cells.ranges[0].max_row > up:
                                find_type = True
                                dn = wb.defined_names.get(name=dV.formula1)
                                range_dn = list(dn.destinations)[0]
                                address_dn = range_dn[1].replace('$', '').split(':')

                                tech_list = wb[range_dn[0]]

                                left_dn = tech_list[address_dn[0]].col_idx
                                up_dn = tech_list[address_dn[0]].row
                                right_dn = tech_list[address_dn[1]].col_idx+1
                                bottom_dn = tech_list[address_dn[1]].row

                                #squared_range_dn = [[o for o in p] for p in tech_list.iter_cols(min_col=left_dn, max_col=right_dn, min_row=up_dn, max_row=bottom_dn)]
                                squared_range_dn = [[o for o in p] for p in tech_list.get_squared_range(left_dn, up_dn, right_dn, bottom_dn)]
                                column_type[i.col_idx] = squared_range_dn

                        guid = ''
                        squared_range_dn_col = column_type.get(i.col_idx)
                        if find_type == True and squared_range_dn_col is not None:
                            for k in squared_range_dn_col:
                                if i.value == k[0].value:
                                    guid = k[1].value
                                    break
                    else:
                        guid = ''
                        squared_range_dn_col = column_type.get(i.col_idx)
                        if squared_range_dn_col is not None:
                            for k in squared_range_dn_col:
                                if i.value == k[0].value:
                                    guid = k[1].value
                                    break

                    value = {}
                    value['value'] = i.value
                    value['guid'] = guid
                    row_rang.append(value)

                data_rang.append(row_rang)

                b += 1

            data.append(
                {'name': definedName.name, 'data': data_rang})
        else:
            bottom = int(address[1])
            up = int(address[0])
            right = int(ws.max_column)
            left = 0

            data_rang = [[i.value for i in j] for j in ws.get_squared_range(left, up, right, bottom)]
            data.append({'name':definedName.name,'data':data_rang, 'left':left, 'up':up, 'right':right, 'bottom':bottom})

    file['wb'].close()
    logging.info("Лист обработан")

    return data, err