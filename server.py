# /usr/bin/python3

import os
import json
import time
import datetime

import excel as xl
import get_region as region_reader
import gevent
import signal
import excel_deserializer

from gevent.pywsgi import WSGIServer
from flask import (request, Flask)

app = Flask(__name__)


@app.route("/service-status", methods=['GET'])
def service_status():
    return "service-status: active"


@app.route("/api/v1/fill_tamplate", methods=['POST'])
def fill_tamplate():

    rq = request.json

    if (rq == None):
        return "Ошибка: не преданы параметры запроса к сервису"

    xl.writeDataToTamplete(rq['xlsx_tample_path'],
                           rq['result_file_name'], rq['value_data'])
    return "OK"


@app.route("/api/v1/get_region", methods=['POST'])
def get_region():

    rq = request.json

    if (rq == None):
        return "request.json is empty"

    region = rq['region']

    variant = rq['read_serialized_array']

    if (variant == None):
        variant = 0

    try:
        result, err = region_reader.get_region_data(
            rq['xlsx_file'], rq['sheet_name'], region['top'], region['left'], region['bottom'], region['right'])
    except:
        return err

    variant_processor = [variant_0, variant_1, variant_2]

    if (variant >= len(variant_processor) or variant < 0):
        return "Неизвестный вариант обработки: {}".format(variant)

    print("Выбран ваиант: {}".format(variant))

    return variant_processor[variant](result)

@app.route("/api/v1/get_names", methods=['POST'])
def get_names():
    rq = request.json

    if (rq == None):
        return "request.json is empty"

    variant = rq['read_serialized_array']

    if (variant == None):
        variant = 0

    try:
        result, err = region_reader.get_names_data(rq['xlsx_file'], rq['sheet_name'])

    except:
        return err

    variant_processor = [variant_0, variant_1, variant_2]

    if (variant >= len(variant_processor) or variant < 0):
        return "Неизвестный вариант обработки: {}".format(variant)

    print("Выбран ваиант: {}".format(variant))

    return variant_processor[variant](result)

def time_stamp(f):
    def wrap(*args, **kwargs):
        begin = time.time()
        f(*args, **kwargs)
        end = time.time()
        print('[*] Время выполнения: {} секунд.'.format(end-begin))
    return wrap


def variant_2(result):
    dthandler = lambda obj: obj.isoformat() if isinstance(obj, datetime.datetime) else None
    return json.dumps(result, ensure_ascii=False, default=dthandler)

def variant_1(result):
    result_xml_tmpl = '''<Array xmlns="http://v8.1c.ru/8.1/data/core"
                        xmlns:xs="http://www.w3.org/2001/XMLSchema"
                        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="Array">
                        {0}
                        </Array>'''

    row_xml_tmpl = '<Value xsi:type="Array">{0}</Value>'
    val_xml_tmpl = '<Value xsi:type="xs:string">{0}</Value>'

    result_xml = result_xml_tmpl.format(
        ''.join([row_xml_tmpl.format(
            ''.join([val_xml_tmpl.format(val) for val in row])) for row in result if row]
        )
    )

    return result_xml


def variant_0(result):
    rows = ['$;$'.join(row)[:-3] for row in result if row]
    result_string = '$%$'.join(rows)[:-3]
    return result_string


@app.route("/api/v1/save_excel_data", methods=['POST'])
def save_excel_data():
    if request.json is not None:
        wb = excel_deserializer.load_workbook(request.json)
        wb.save(filename=request.json["book_name"])
    return "Ok"

@app.route("/api/v1/save_excel_template", methods=['POST'])
def save_excel_template():
    rq = request.json

    if (rq == None):
        return "request.json is empty"
    try:
        result, err = xl.save_excel_template(rq['xlsx_file'], rq['sheets'], rq['enums'])

    except:
        return err

    return "Ok"

def main():
    try:
        host = os.environ.get('excel_host', '0.0.0.0')
        port = int(os.environ.get('excel_port', 5125))
        print('Starting up on %s:%d' % (host, port))
        http_server = WSGIServer((host, port), app)

        def stop():
            print('Handling signal')
            if http_server.started:
                http_server.stop()

        gevent.signal(signal.SIGTERM, stop)
        http_server.serve_forever()

    except (KeyboardInterrupt, SystemExit):
        print('Stopping')
        if http_server.started:
            http_server.stop()
    finally:
        print('Stoped')


if __name__ == "__main__":
    main()
    # Debug/Development
    #app.run(host=excel_host, port=excel_port)
