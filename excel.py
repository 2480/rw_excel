from openpyxl import Workbook
from openpyxl import load_workbook
from openpyxl.styles import Font, Color, PatternFill
from openpyxl.styles.borders import Border, Side
from openpyxl import workbook
from openpyxl.worksheet.datavalidation import DataValidation

tamplate_path   = "/tmp/"
result_path     = "/tmp/"     

def return_ws(tamplate_name):
    wb = load_workbook(tamplate_path + tamplate_name)
    ws = wb.active
    return {'wb': wb, 'ws': ws}


# Wtite date to xlsx tamplate
def writeDataToTamplete(tamplate_name, result_name, data):

    xlsx = return_ws(tamplate_name)
    
    for el in data:
        xlsx['ws'].cell(el[0], el[1]).value = el[2]

    xlsx['wb'].save(result_path + result_name)

def save_excel_template(xlsx_file, sheets, enums):

    wb = Workbook()

    ws = wb.worksheets[0]
    ws.title = 'Тех_лист'

    wb.active

    start_row = 1
    start_column = 1
    cur_column = start_column

    for enum in enums:
        cur_row = start_row
        for i in enum['value']:
            cell = ws.cell(cur_row, cur_column)
            cell.value = i['presentation']
            cell = ws.cell(cur_row, cur_column+1)
            cell.value = i['guid']
            cur_row += 1

        column_defined_name = ws.cell(start_row, cur_column).column

        attr_text = ws.title + '!$' + column_defined_name + '$' + str(start_row) + ':$' + column_defined_name+ '$' + str(cur_row-1)
        new_range = workbook.defined_name.DefinedName("Список_" + enum['key'].replace('.', ''), attr_text=attr_text)
        wb.defined_names.append(new_range)

        cur_column = cur_column + 2

    for sheet in sheets:

        ws = wb.create_sheet(title=sheet['sheet_name'], index=0)

        wb.active

        start_row_column = sheet['start_row_column']
        start_row = start_row_column['row']
        start_column = start_row_column['column']

        form_title = sheet['form_title']

        form_title_cell = ws.cell(start_row, start_column)
        form_title_cell.value = form_title['value']
        form_title_cell.font = Font(size="24")

        cur_row = start_row + 2

        for attribute in sheet['attributes']:

            attribute_cell = ws.cell(cur_row, start_column)
            attribute_cell.value = attribute['title']

            if cur_row == start_row + 2:
                border = Border(left=Side(style='medium'),
                                    top=Side(style='medium'))
            elif len(sheet['attributes']) == cur_row + 1 - start_row - 2:
                border = Border(left=Side(style='medium'),
                                     bottom=Side(style='medium'))
            else:
                border = Border(left=Side(style='medium'))

            attribute_cell.border = border

            column_defined_name = ws.cell(cur_row, start_column + 1).column

            attr_text = ws.title + '!$' + column_defined_name + '$' + str(cur_row)
            new_range = workbook.defined_name.DefinedName("Область_"+attribute['name'], attr_text=attr_text)
            wb.defined_names.append(new_range)

            if cur_row == start_row + 2:
                border = Border(right=Side(style='medium'),
                                     top=Side(style='medium'))
            elif len(sheet['attributes']) == cur_row + 1 - start_row - 2:
                border = Border(right=Side(style='medium'),
                                     bottom=Side(style='medium'))
            else:
                border = Border(right=Side(style='medium'))

            cell = ws.cell(cur_row, start_column+1)
            cell.border = border
            cell.fill = PatternFill(start_color="CFCFCF", fill_type="solid")

            for i in enums:
                if i['key'] == attribute['type']:
                    formula1 = "Список_" + i['key'].replace('.','')
                    dv = DataValidation(type="list", formula1=formula1, allow_blank=True)
                    ws.add_data_validation(dv)
                    cell = ws.cell(cur_row, start_column + 1)
                    dv.add(cell)

            cur_row = cur_row + 1

        cur_row = cur_row + 1
        cell = ws.cell(cur_row, start_column)
        cell.value = sheet['table']['title_table']
        cell.font = Font(size="16")

        cur_row = cur_row + 1
        cur_column = start_column

        for attribute in sheet['table']['attributes']:

            attribute_cell = ws.cell(cur_row, cur_column)
            attribute_cell.value = attribute['title']

            border = Border(bottom=Side(style='thin'),
                            left=Side(style='thin'),
                            right=Side(style='thin'),
                            top=Side(style='thin'))

            attribute_cell.border = border
            attribute_cell.fill = PatternFill(start_color="0000FF", fill_type="solid")
            attribute_cell.font = Font(color="FFFFFF")

            for i in enums:
                if i['key'] == attribute['type']:
                    formula1 = "Список_" + i['key'].replace('.','')
                    dv = DataValidation(type="list", formula1=formula1, allow_blank=True)
                    ws.add_data_validation(dv)
                    cell_up = ws.cell(cur_row+1, cur_column)
                    cell_bottom = ws.cell(cur_row+500000, cur_column)

                    dv.add(cell_up.coordinate+':'+cell_bottom.coordinate)

            cur_column = cur_column + 1

        column_defined_name_left = ws.cell(cur_row, start_column).column
        column_defined_name_right = ws.cell(cur_row, cur_column-1).column

        attr_text = ws.title + '!$' + column_defined_name_left + '$' + str(cur_row) + ':$' + column_defined_name_right + '$' + str(cur_row)
        new_range = workbook.defined_name.DefinedName("Область_" + sheet['table']['name_table'], attr_text=attr_text)
        wb.defined_names.append(new_range)

        y = 0
        for column_cells in ws.columns:
            length = max(len(as_text(cell.value)) for cell in column_cells)
            if y == 0:
                ws.column_dimensions[column_cells[0].column].width = length+5
            else:
                ws.column_dimensions[column_cells[0].column].width = length+15

            y += 1

    try:
        wb.save(result_path + xlsx_file) # xlsx_file
        result = 'ok'
        err = {'code': 0}
    except:
        result = 'error'
        err = {'code': 1, 'msg': "Файл: " + result_path + xlsx_file + " не сохранен!"}

    wb.close()

    return result, err

def as_text(value):
    if value is None:
        return ""
    return str(value)

