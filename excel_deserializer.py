# -*- coding: utf-8 -*-

import json
import codecs
import os.path

from openpyxl import Workbook
from openpyxl.styles import (
    PatternFill,
    Font,
    Border,
    Side,
    Alignment,
    NamedStyle,)


_thin = Side(border_style="thin", color="000000")
BORDER_THIN = Border(top=_thin, left=_thin, right=_thin, bottom=_thin)


def detect_encoding(data):
    head = data[:4]
    if head[:3] == codecs.BOM_UTF8:
        return 'utf-8-sig'
    return 'utf-8'


def deserialize_data(s):
    data = None
    s = None
    filepath = r"d:\data.json"
    if os.path.exists(filepath):
        with open(filepath, "rb") as fp:
            s = fp.read()
            encoding = detect_encoding(s)
            s = s.decode(encoding)
        if s is not None:
            data = json.loads(s)
    return data


def get_styles(styles, style_description):
    for name, descr in style_description.items():
        if name in styles.keys():
            continue

        style = NamedStyle(name=name)

        top = Side(style=descr["btop"], color="000000")
        left = Side(style=descr["bleft"], color="000000")
        right = Side(style=descr["bright"], color="000000")
        bottom = Side(style=descr["bbottom"], color="000000")
        style.border = Border(top=top, left=left, right=right, bottom=bottom)

        style.number_format = descr["number_format"]

        if descr["fgColor"] is not None:
            style.fill = PatternFill("solid", fgColor=descr["fgColor"])

        style.alignment = Alignment(horizontal=descr["ahorizontal"], vertical=descr["avertical"],
                                    textRotation=0, wrapText=descr["awrap_text"], shrinkToFit=False)
        style.font = Font(
            name=descr["fname"], size=descr["fsize"], b=descr["fbold"], color=descr["fcolor"])

        styles[name] = style

    return styles


def set_merge(ws, merge):
    ws.merge_cells(merge)
    merged_cells = (cell for cells in ws[merge] for cell in cells)
    for cell in merged_cells:
        cell.border = BORDER_THIN


def set_group(ws, grp):
    if grp["type"] == "hg":
        ws.column_dimensions.group(
            grp["start"], grp["end"], grp["level"], False)
    elif grp["type"] == "vg":
        ws.row_dimensions.group(grp["start"], grp["end"], grp["level"], False)


def set_sheet_properties(ws, properties):
    if properties is not None:
        ws.sheet_properties.outlinePr.summaryRight = properties.get(
            "summaryRight", True)


def set_data_and_style(ws, data, styles):
    for i in range(len(data)):
        for col, v in data[i].items():
            cell_name = "{}{}".format(col, i+1)
            cell = ws[cell_name]

            try:
                val = float(v["val"])
            except ValueError:
                val = v["val"]

            cell.value = val
            style = styles.get(v["style_id"])
            if style is not None:
                cell.style = style


def fill_worksheets(wb, sheets):
    wb_styles = {}
    for sheet in sheets:
        ws = wb.create_sheet(sheet["title"])
        get_styles(wb_styles, sheet["styles"])
        sheet_data = sheet["data"]
        properties = sheet["properties"]

        set_data_and_style(ws, sheet_data, wb_styles)

        for merge in sheet["merges"]:
            set_merge(ws, merge)

        for g in sheet["groups"]:
            set_group(ws, g)

        set_sheet_properties(ws, properties)


def load_workbook(data):
    wb = Workbook()
    for ws in wb:
        wb.remove(ws)
    fill_worksheets(wb, data["sheets"])
    return wb
